import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
// import { Connection } from 'typeorm';

import { AppController } from './app.controller';
import { UsersModule } from './users/users.module';

@Module({
  controllers: [AppController],
  imports: [
    TypeOrmModule.forRoot(), // When ForRoot() is left empty it loads from ormconfig.json
    UsersModule
  ]
})
export class ApplicationModule {
  // constructor(private readonly connection: Connection){}
}
